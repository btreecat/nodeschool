const http = require('http');
const map = require('through2-map');

//console.log(text);
var server = http.createServer(function(request, response) {
        request.pipe(map(function (chunk) {
            return chunk.toString().toUpperCase();
        })).pipe(response);
    });
server.listen(8000);
