const bl = require('bl');
const http = require('http');

var urls = [process.argv[2], process.argv[3], process.argv[4]]
var count = 3;
var results = {};

urls.forEach(function (url) {
    http.get(url, function(response) {

        response.pipe(bl(function(err, data) {
            results[url] = data;
            //console.log(data.toString());
            count--;
            if (count <= 0) {
                urls.forEach(function (u) {
                    console.log(results[u].toString());
                });
            }
        }));
    });

});
