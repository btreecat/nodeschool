const http = require('http');
const moment = require('moment');
const url = require('url');

var server = http.createServer(function(request, response) {

        var req = url.parse(request.url, true);
        //console.log(req.pathname);
        var time;
        if (req.query.iso) {
            time = moment(req.query.iso);
        }
        else {
            time = moment();
        }
        response.writeHead(200, { 'Content-Type': 'application/json' });

        if (req.pathname == "/api/parsetime") {

            var reply = {"hour":time.hour(), "minute":time.minute(), "second":time.second()};
            response.end(JSON.stringify(reply));
        }
        if (req.pathname == "/api/unixtime") {
            response.end(JSON.stringify({"unixtime": time.valueOf()}));
        }



    });
server.listen(8000);
