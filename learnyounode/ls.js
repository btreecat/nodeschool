var fs = require('fs');

var dir = process.argv[2];
var filter = '';
if (process.argv.length > 3) {
    filter = process.argv[3];
}
fs.readdir(dir, function(err, list) {
    for (file in list) {
        if (filter != '') {
            var filter_str = "\\." + filter + "$";
            //console.log(filter_str);
            var re = new RegExp(filter_str);

            if (list[file].match(re)) {
                console.log(list[file]);
            }
        }
        else {
            console.log(list[file]);
        }
    }
});
