const net = require('net');
const moment = require('moment');

var server = net.createServer(function(socket) {
        console.log('server connected');
        console.log(socket.remoteAddress);
        socket.end(moment().format("YYYY-MM-DD HH:mm") + "\n");
    });
server.listen(8000)
