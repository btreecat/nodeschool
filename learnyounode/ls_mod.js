var fs = require('fs');

module.exports = ls;

function ls(dir, filter, callback) {

    var result = [];

    filter = filter || "";

    fs.readdir(dir, function(err, list) {
        if (err){
            return callback(err);
        }
        for (file in list) {
            if (filter != '') {
                var filter_str = "\\." + filter + "$";
                //console.log(filter_str);
                var re = new RegExp(filter_str);

                if (list[file].match(re)) {
                    //console.log(list[file]);
                    result.push(list[file]);
                }
            }
            //if no filter was supplied
            else {
                result.push(list[file]);
            }
        }

        return callback(null, result);
    });


}

