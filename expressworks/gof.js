var express = require('express');
var app = express();
app.set('view engine', 'jade');
app.set('views', process.argv[3]);
app.use(express.urlencoded());
app.get('/', function(req, res) {
    res.end('Hello Root World!');
});
app.get('/home', function(req, res) {
    res.render('index', {date: new Date().toDateString()});
});
app.post('/form', function(req, res) {
    var str = req.body.str.split('').reverse().join('');
    res.end(str);
});
app.listen(process.argv[2]);
